#!/bin/sh

INPUT=$(ibus engine)


if [ "$INPUT" = "libpinyin" ]; then
    echo "  中文 "
elif [ "$INPUT" = "xkb:us::eng" ]; then
    echo "  en "
elif [ "$INPUT" = "rime" ]; then
    echo "  語 "
else
    echo "IBusOff"
fi
