#!/bin/sh

STATUS=$(nordvpn status | grep Status | tr -d ' ' | cut -d ':' -f2)
SERVER=$(nordvpn status | grep server | sed "s/Current\ server:\ //" | sed "s/.nordvpn.com//")

if [ "$STATUS" = "Connected" ]; then
    echo " " $SERVER ""
else
    echo "  OFFLINE "
fi
