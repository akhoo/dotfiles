# dotfiles

These are the dotfiles+instructions for installing arch and setting up `i3-gaps` on a Xiaomi mi notebook air 13. Heavily inspired by the amazing people over at /r/unixporn. Visit the [archwiki page](https://wiki.archlinux.org/index.php/Xiaomi_Mi_Notebook_Air_13.3) for specific information on this model and setting it up.

`git clone` this repo to `~/dotfiles`. Then `cd` into it and `stow <folder>`. Obviously requires `stow` installed first.


## Tl;dr

 -  WM: i3
 -  bar: polybar
 -  Icons: candy
 -  GTK: sweet folders purple
 -  Wallpaper: unsplash stuff
 -  Term: rxvt-unicode


# Todo:

1.  Copy vim settings from other computer for that markdown syntax
2.  Keybindings: remove that stupid airplane mode and re-fix prtscn
3.  Polybar: the module internal/volume is deprecated so be prepared to fix it when it breaks
4.  Make the super button alone the change keyboard button in ibus


# Arch Installation

Install arch as usual using the wiki. Although there is a lot of stuff written here, this is not meant to be a guide, but rather just a compilation of steps that I have had issues with on previous installations, to streamline my next installation.

## Partitioning

Set up the partitions using `cfdisk /dev/nvme0n1`

-  500M EFI
-  4G swap
-  30 G linux root (x86-64)
-  Rest linux filesystem

Set up the partitions and mount them using (order matters; replace with the actual number partition, e.g. nvm-root = root = nvme0n1p3):

-  `mkfs.ext4 /dev/nvm-root`
-  `mkfs.ext4 /dev/nvm-home`
-  `mkswap /dev/nvm-swap`
-  `swapon /dev/nvm-swap`
-  `mount /dev/nvm-root /mnt`
-  `mkdir /mnt/home`
-  `mount /dev/nvm-home /mnt/home`
-  `mkdir /mnt/boot`
-  `mount /dev/nvm-boot /mnt/boot`: might have to `rm -r /mnt/boot` to delete stuff from it if there are leftovers from a previous installation

## Initial stuff to install

Comment all the mirrors by `sed -i -e 's/^/#/' /etc/pacman.d/mirrorlist`

`pacstrap base base-devel`: devel is necessary later on

`pacman -S dialog iw wpa_supplicant os-prober grub efibootmgr`

-  `dialog` `iw` `wpa_supplicant` are necessary for wireless internet connectivity
-  `os-prober grub efibootmgr` obviously to get grub working for EFI and dual boot

`pacman -S vim sudo xorg xorg-xinit`

## Grub issues

`grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub`

`--target=i386` is WRONG for this system.

`grub-mkconfig -o /boot/grub/grub.cfg` after that step regarding `intel-ucode` (?) on the installation guide


# Post installation

MAKE SURE LOCALE IS US ELSE AU MAKES ERRORS

## Basic functionality

`pacman -S git rxvt-unicode stow feh rofi thunar thunar-volman gvfs udiskie firefox atom arandr xfce4-screenshooter alsa-utils poppler tlp acpi redshift wireless_tools keepassxc i3 vlc`

-  `git` is git
-  `stow` is to create symlinks with dotfiles
-  `rxvt-unicode`: is the terminal
-  `feh` for background
-  `rofi` opens everything
-  `thunar` file explorer
-  `thunar-volman gvfs udiskie` automounts usbs. Don't forget to edit > preferences > advanced > enable vol management > configure > mount..
-  `firefox`
-  `atom` text editor
-  `arandr` GUI for xrandr, can use `xrandr --output HDMI2 --auto` if don't want GUI
-  `xfce4-screenshooter` the best screenshotting tool in existence
-  `alsa-utils` getting sound working
-  `poppler` contains `pdfunite pdfseparate` for my shell scripts
-  `tlp` battery saving
-  `acpi` allows battery status to be read
-  `redshift` so your eyes can survive
-  `wireless_tools` lets polybar detect internal/network
-  `keepassxc` password manager
-  `i3` is the tiling windows manager
-  `vlc` is media player

Set up `yay` using [this link](https://www.ostechnix.com/yay-found-yet-another-reliable-aur-helper/) (RIP yaourt)

`yay -S ttf-inconsolata ttf-font-awesome-4 polybar`

-  `ttf-inconsolata` is terminal font
-  `ttf-font-awesome-4` for fontawesome v4.7 which is in the polybar
-  `polybar` make sure this is installed *last* after everything else in order to ensure the stuff works/is detected.


## Setting up

### Check archlinux wiki for model

[Arch wiki xiaomi mi notebook](https://wiki.archlinux.org/index.php/Xiaomi_Mi_Notebook_Air_13.3)

Install graphics card to get brightness adjustment working - RTFM

`pacman -S xf86-video-intel xf86-video-nouveau bbswitch`

-  `xf86-video-intel` driver for intel GPU
-  `xf86-video-nouveau` driver for extra GPU. Pretty sure its necessary to install this else X breaks
-  `bbswitch` allows us to disable the extra GPU (follow archwiki link)


### Time

-  `timedatectl set-ntp true`
-  `timedatectl set-timezone Country/place`

### Internet

`wifi-menu -o` to save profiles.

Autostart netctl `sudo systemctl enable netctl-auto@wlp2s0.service` to start internet connection on boot.

Add the `uni` profile to `/etc/netctl/` and edit with username to access **eduroam**@UQ.

### Asian languages working

Check [this link](https://en.wikipedia.org/wiki/Help:Multilingual_support_(East_Asian)) for more info

`sudo pacman -S adobe-source-han-sans-otc-fonts ibus-libpinyin` followed by `fc-cache -vf`

-  `adobe-source-han-sans-otc-fonts` for chinese input
-  `ibus-libpinyin` is the keyboard input for chinese see [ibus wikilink](https://wiki.archlinux.org/index.php/IBus). In `ibus-setup` make sure its <Super>Space and remove (inside chinese keyboard) the shift to switch languages

### Battery saving

`systemctl enable tlp.service` and `tlp-sleep.service`. Check out tlp archlinux wiki page.

### Dotfiles

`git clone` this repo to `~/dotfiles` then `cd dotfiles`.

`stow Xstow` and the rest of the folders to create symlinks.

`sudo cp -r Xcopy/xorg.conf.d /etc/X11/` to copy the keyboard and screensize settings.

### Wallpaper

Visit [unsplash](https://unsplash.com/) to download some nice pictures and put them in `~/Pictures/Wallpapers`

### Firefox

-  Ublock origin
-  HTTPS everywhere
-  After setting gtk, follow [this](https://www.mkammerer.de/blog/gtk-dark-theme-and-firefox/) to make text boxes legible
-  Keepasxc connector
-  No script if you are bothered
-  [Ctrl-tab behaviour](https://superuser.com/questions/1433547/how-to-get-old-tab-switching-behavior-back-in-firefox)
-  ?`pacman -S flashplugin`

### VPN

Set up nordvpn as per [here](https://wiki.archlinux.org/index.php/NordVPN)


## Extra things that are used but not necessary

`pacman -S r cmatrix pdfpc ?gcc-fortran libreoffice-still lxappearance tranmission-gtk`

`yay -S rstudio-desktop-bin foxitreader neofetch bash-pipes ?ttf-fira-sans ?ttf-fira-mono zotero pandoc-bin`

-   fira sans is font for `\usetheme{metropolis}` not sure if I need both
-  `transmission-gtk` is the torrent
-  `rstudio-desktop-bin` zoom 150%, editor font size 12, material
-  `pandoc` is annoying since it is so fat but it is needed for rendering documents and im too lazy for the small install
-  `zotero` for referencing and install [zotero connector](https://www.zotero.org/download/connectors) in firefox
-  `pandoc-bin` instead of `pandoc` because it doesn't come with the whole haskell dependencies

`texlive`: [arch wiki here](https://wiki.archlinux.org/index.php/TeX_Live#Manual_installation), install small + chinese + w/e

-  `tar -xvzf install-tl-unx.tar.gz`
-  `sudo perl install-tl -gui` after `sudo pacman -S perl-tk wget`

Install `vundle` (from github)

## GTK theme + icon pack

-  GTK: [sweet dark](https://www.gnome-look.org/p/1253385/). Extract `tar -xJf filename` and copy to `/usr/share/themes/`
-  Icon pack: [sweet folders](https://www.opendesktop.org/p/1284047/) ?change
-  Firefox: [sweet theme](https://addons.mozilla.org/es/firefox/addon/sweet-dark/)

If `lxappearance` is not showing then check permissions and `chmod 755` it

# Screenshots

![Busy](https://imgur.com/FSWUFml.png)

![Clean](https://imgur.com/DRlRRyX.png)
